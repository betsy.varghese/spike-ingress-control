# Use the official maven/Java 8 image to create a build artifact.
# https://hub.docker.com/_/maven
FROM maven:3.6-jdk-11 as builder

# Copy local code to the container image.
WORKDIR /app
COPY pom.xml .
COPY src ./src

# Build a release artifact.
RUN mvn package -DskipTests

# Use AdoptOpenJDK for base image.
# It's important to use OpenJDK 8u191 or above that has container support enabled.
# https://hub.docker.com/r/adoptopenjdk/openjdk8
# https://docs.docker.com/develop/develop-images/multistage-build/#use-multi-stage-builds

FROM adoptopenjdk/openjdk11:alpine-slim

# Copy the jar to the production image from the builder stage.
COPY --from=builder /app/target/helloworld-*.jar /helloworld.jar

# Cloud Profiler 
RUN mkdir -p /opt/cprof && \
  wget -q -O- https://storage.googleapis.com/cloud-profiler/java/latest/profiler_java_agent.tar.gz \
  | tar xzv -C /opt/cprof

# Cloud Debugger (old)
# Create a directory for the Debugger. Add and unzip the agent in the directory.
RUN mkdir /opt/cdbg && \
     wget -qO- https://storage.googleapis.com/cloud-debugger/compute-java/debian-wheezy/cdbg_java_agent_gce.tar.gz | \
     tar xvz -C /opt/cdbg


# Cloud debugger agent build new

#RUN git clone https://github.com/GoogleCloudPlatform/cloud-debug-java

#WORKDIR cloud-debug-java
#RUN bash build.sh
#RUN mkdir -p /opt/cdbg
#RUN tar -xvf cdbg_java_agent_service_account.tar.gz -C /opt/cdbg

# Start the debugging agent when the app is deployed.

#CMD [ "java",  "-agentpath:/opt/cdbg/cdbg_java_agent.so", "-agentpath:/opt/cprof/profiler_java_agent.so=-cprof_service=${K_SERVICE}", "-Djava.security.egd=file:/dev/./urandom", "-Dcom.google.cdbg.module=${K_SERVICE}" , "-Dcom.google.cdbg.version=${K_REVISION}" , "-Dcom.google.cdbg.breakpoints.enable_canary=true", "-jar",  "/helloworld.jar" ] 
CMD [ "java",  "-agentpath:/opt/cdbg/cdbg_java_agent.so", "-agentpath:/opt/cprof/profiler_java_agent.so=-cprof_service=helloworld", "-Djava.security.egd=file:/dev/./urandom",  "-Dcom.google.cdbg.breakpoints.enable_canary=true", "-jar",  "/helloworld.jar" ] 

# debugger
#COPY --from=0 /opt/cdbg /opt/cdbg
